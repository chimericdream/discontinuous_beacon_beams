package net.sssubtlety.discontinuous_beacon_beams;

import de.guntram.mcmod.crowdintranslate.CrowdinTranslate;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DiscontinuousBeaconBeams {
    public static final String NAMESPACE = "discontinuous_beacon_beams";
    public static final Logger LOGGER = LogManager.getLogger();

    public static class ClientInit implements ClientModInitializer {
        @Override
        @Environment(EnvType.CLIENT)
        public void onInitializeClient () {
            CrowdinTranslate.downloadTranslations("discontinuous-beacon-beams", NAMESPACE);
        }
    }
}
