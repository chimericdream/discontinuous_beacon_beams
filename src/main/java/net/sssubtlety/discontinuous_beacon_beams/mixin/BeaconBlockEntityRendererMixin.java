package net.sssubtlety.discontinuous_beacon_beams.mixin;

import net.minecraft.block.entity.BeaconBlockEntity;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BeaconBlockEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.sssubtlety.discontinuous_beacon_beams.mixin_accessors.BeamSegmentMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.List;

import static net.sssubtlety.discontinuous_beacon_beams.DiscontinuousBeaconBeamsConfig.enableBeaconBeams;

@Mixin(BeaconBlockEntityRenderer.class)
public abstract class BeaconBlockEntityRendererMixin {
    private boolean curSegmentIsVisible;

    @Inject(method = "render(Lnet/minecraft/block/entity/BeaconBlockEntity;FLnet/minecraft/client/util/math/MatrixStack;Lnet/minecraft/client/render/VertexConsumerProvider;II)V",
            locals = LocalCapture.CAPTURE_FAILEXCEPTION, at = @At(value = "INVOKE",
            target = "Lnet/minecraft/client/render/block/entity/BeaconBlockEntityRenderer;renderBeam(Lnet/minecraft/client/util/math/MatrixStack;Lnet/minecraft/client/render/VertexConsumerProvider;FJII[F)V"))
    private void checkInvisibleSegment(BeaconBlockEntity beaconBlockEntity, float f, MatrixStack matrixStack, VertexConsumerProvider vertexConsumerProvider, int arg4, int arg5, CallbackInfo ci, long l, List list, int k, int m, BeaconBlockEntity.BeamSegment beamSegment) {
        curSegmentIsVisible = enableBeaconBeams() && ((BeamSegmentMixinAccessor)beamSegment).isVisible();
    }

    @ModifyArg(method = "render(Lnet/minecraft/block/entity/BeaconBlockEntity;FLnet/minecraft/client/util/math/MatrixStack;Lnet/minecraft/client/render/VertexConsumerProvider;II)V",
            index = 0, at = @At(value = "INVOKE",
                    target = "Lnet/minecraft/client/render/block/entity/BeaconBlockEntityRenderer;renderBeam(Lnet/minecraft/client/util/math/MatrixStack;Lnet/minecraft/client/render/VertexConsumerProvider;FJII[F)V"))
    private MatrixStack passNullMatrixIfInvisible(MatrixStack matrixStack) {
        return curSegmentIsVisible ? matrixStack : null;
    }

    @Inject(method = "renderBeam(Lnet/minecraft/client/util/math/MatrixStack;Lnet/minecraft/client/render/VertexConsumerProvider;FJII[F)V",
            cancellable = true, at = @At(value = "HEAD"))
    private static void skipInvisibleBeams(MatrixStack matrixStack, VertexConsumerProvider vertexConsumerProvider, float f, long l, int i, int j, float[] fs, CallbackInfo ci) {
        if (matrixStack == null) ci.cancel();
    }
}
