package net.sssubtlety.discontinuous_beacon_beams;

import net.minecraft.block.entity.BeaconBlockEntity;

public class InvisibleBeamSegment extends BeaconBlockEntity.BeamSegment {
    private final boolean precededByColor;
    public InvisibleBeamSegment(float[] color, boolean precededByColor) {
        super(color);
        this.precededByColor = precededByColor;
    }

    public boolean wasPrecededByColor() {
        return precededByColor;
    }
}
