package net.sssubtlety.discontinuous_beacon_beams.mixin;

import net.minecraft.block.entity.BeaconBlockEntity;
import net.sssubtlety.discontinuous_beacon_beams.mixin_accessors.BeamSegmentMixinAccessor;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(BeaconBlockEntity.BeamSegment.class)
public class BeamSegmentMixin implements BeamSegmentMixinAccessor {
    @Shadow @Final @Mutable float[] color;
    private boolean visible;
    private boolean hasColor;

    @Inject(method = "<init>", at = @At("TAIL"))
    private void init(CallbackInfo ci) {
        visible = true;
        hasColor = true;
    }

    @Override
    public void setInvisible() {
        visible = false;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

//    @Override
//    public void setNoColor() {
//        hasColor = false;
//    }

//    @Override
//    public boolean hasColor() {
//        return hasColor;
//    }

    @Override
    public void setColor(float[] color) {
        this.color = color;
    }
}
