package net.sssubtlety.discontinuous_beacon_beams;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.annotation.ConfigEntry;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.List;

import static net.sssubtlety.discontinuous_beacon_beams.DiscontinuousBeaconBeams.LOGGER;
import static net.sssubtlety.discontinuous_beacon_beams.DiscontinuousBeaconBeams.NAMESPACE;

@Config(name = NAMESPACE)
public class DiscontinuousBeaconBeamsConfig implements ConfigData {
    private static final DiscontinuousBeaconBeamsConfig INSTANCE = AutoConfig.register(DiscontinuousBeaconBeamsConfig.class, GsonConfigSerializer::new).getConfig();
    private static ImmutableSet<Block> beamHidingBlockSet = blockSetFromStrings(INSTANCE.beamHidingBlocks);

    @ConfigEntry.Gui.Excluded
    private static int beamHidingBlocksHash = INSTANCE.beamHidingBlocks.hashCode();

    public static boolean doesHideBeam(Block block) {
        final int currentHash = INSTANCE.beamHidingBlocks.hashCode();
        if (beamHidingBlocksHash != currentHash) {
            beamHidingBlocksHash = currentHash;
            beamHidingBlockSet = blockSetFromStrings(INSTANCE.beamHidingBlocks);
        }

        return beamHidingBlockSet.contains(block);
    }

    public static boolean hidingBlocksToggleBeams() {
        return INSTANCE.hidingBlocksToggleBeams;
    }

    public static boolean rememberColorWhenHidingBlocksEnableBeams() {
        return INSTANCE.rememberColorWhenHidingBlocksEnableBeams;
    }

    public static boolean stainableBlocksEnableBeams() {
        return INSTANCE.stainableBlocksEnableBeams;
    }

    public static boolean rememberColorWhenStainableBlocksEnableBeams() {
        return INSTANCE.rememberColorWhenStainableBlocksEnableBeams;
    }

    public static boolean enableBeaconBeams() {
        return INSTANCE.enableBeaconBeams;
    }

    @ConfigEntry.Gui.Tooltip
    private List<String> beamHidingBlocks = Lists.newArrayList(
            Registry.BLOCK.getId(Blocks.GLASS).toString(),
            Registry.BLOCK.getId(Blocks.GLASS_PANE).toString()
    );

    @ConfigEntry.Gui.Tooltip
    private boolean hidingBlocksToggleBeams = false;

    @ConfigEntry.Gui.Tooltip
    private boolean rememberColorWhenHidingBlocksEnableBeams = true;

    @ConfigEntry.Gui.Tooltip
    private boolean stainableBlocksEnableBeams = false;

    @ConfigEntry.Gui.Tooltip
    private boolean rememberColorWhenStainableBlocksEnableBeams = true;

    @ConfigEntry.Gui.Tooltip
    private boolean enableBeaconBeams = true;

    private static ImmutableSet<Block> blockSetFromStrings(Iterable<String> blockIdStrings) {
        ImmutableSet.Builder<Block> blockSetBuilder = ImmutableSet.builder();
        for (String string : blockIdStrings) {
            Identifier id = new Identifier(string);

            Block block = Registry.BLOCK.get(id);
            if (block == Blocks.AIR)
                LOGGER.error("No block found for id " + id);
            else
                blockSetBuilder.add(block);
        }

        return blockSetBuilder.build();
    }
}
