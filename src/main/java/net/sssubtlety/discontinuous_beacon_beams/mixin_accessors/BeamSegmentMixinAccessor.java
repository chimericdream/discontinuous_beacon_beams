package net.sssubtlety.discontinuous_beacon_beams.mixin_accessors;

public interface BeamSegmentMixinAccessor {
    void setInvisible();
    boolean isVisible();
//    void setNoColor();
//    boolean hasColor();
    void setColor(float[] color);
}
