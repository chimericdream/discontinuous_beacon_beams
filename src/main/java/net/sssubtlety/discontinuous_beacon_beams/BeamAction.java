package net.sssubtlety.discontinuous_beacon_beams;

public enum BeamAction {
    NONE,
    HIDE,
    RESET,
    RESTORE
}
