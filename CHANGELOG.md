- 1.1.1 (13 Dec. 2021): Marked as compatible with 1.18.1
- 1.1 (7 Dec. 2021):

  Updated for 1.18!

  Configs no longer require a restart to take effect!

  Added new options!
    - Hiding blocks toggle beams (defaults to false): If an invisible beam passes through a block that hides beams, the beam becomes visible again.
    - Remember color when hiding blocks enable beams (defaults to true): When a block that hides beams makes a beam visible again, remember the beam color from before it became invisible.
    - Stained blocks enable beams (defaults to false): If an invisible beam passes through a block that changes beams' colors, the beam becomes visible again.
    - Remember color when dyed blocks enable beams (defaults to true): When a block that changes beams' colors makes a beam visible again, remember the beam color from before it became invisible.
    - Enable beacon beams (defaults to true): Enable or completely disable beacon beams.

- 1.0.4 (9 Jul. 2021): Marked as compatible with 1.17.1.
- 1.0.3-1 (15 Jun. 2021): Added missing dependency declarations so that fabric will warn you if you start the game without required dependencies, rather than crashing later on. 
- 1.0.3 (15 Jun. 2021): Updated for 1.17.
- 1.0.2 (16 Jan. 2021): Marked as compatible with 1.16.5.
- 1.0.1 (11 Jan. 2021): Fixed translation downloads.
- 1.0 (27 Dec. 2020): Initial release.
- 0.0.1 (25 Dec. 2020): Initial version.