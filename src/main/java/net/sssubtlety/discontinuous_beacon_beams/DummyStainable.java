package net.sssubtlety.discontinuous_beacon_beams;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.Stainable;
import net.minecraft.util.DyeColor;

public class DummyStainable extends Block implements Stainable {
    public DummyStainable() {
        super(FabricBlockSettings.copy(Blocks.AIR));
    }

    @Override
    public DyeColor getColor() {
        return DyeColor.WHITE;
    }
}
