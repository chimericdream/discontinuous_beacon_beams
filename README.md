This mod is for the [Fabric mod loader](https://www.fabricmc.net/d) and [![minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_discontinuous-beacon-beams_all.svg)](https://www.curseforge.com/minecraft/mc-mods/discontinuous-beacon-beams/files)

Beacon beams won't render above clear glass or glass panes in their path. 

They'll begin rendering again when they encounter another transparent colored block in their path. 

All changes are purely visual, this mod doesn't allow beacon beams to go through blocks they usually couldn't go through (full, non-transparent blocks).

![discontinuous_beacon_beams_example](https://gitlab.com/supersaiyansubtlety/discontinuous_beacon_beams/-/raw/master/discontinuous_beacon_beams_example.png)

The list of blocks that can hide beacon beams can be configured either through [Mod Menu](https://www.curseforge.com/minecraft/mc-mods/modmenu/files/all?filter-game-version=1738749986%3a70886) or by editing `.minecraft/config/discontinuous_beacon_beams.json`

By default, only clear glass and glass panes are in the list. 

This mod is purely client-side, it won't do anything if installed on a server. 

If you'd like to contribute a translation, follow [this link](https://crowdin.com/project/mod-id). 
Translations will be available once approved without the need to update the mod thanks to [CrowdinTranslate](https://github.com/gbl/CrowdinTranslate).

[![Requires the Fabric API](https://i.imgur.com/Ol1Tcf8.png)](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files/all?filter-game-version=1738749986%3a70886)

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it. 

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required. 
