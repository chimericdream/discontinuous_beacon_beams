package net.sssubtlety.discontinuous_beacon_beams.mixin_accessors;

public interface BeaconBlockEntityMixinAccessor {
    boolean beamIsVisible();
}
