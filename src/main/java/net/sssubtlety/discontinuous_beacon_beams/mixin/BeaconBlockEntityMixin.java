package net.sssubtlety.discontinuous_beacon_beams.mixin;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.Stainable;
import net.minecraft.block.entity.BeaconBlockEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.DyeColor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.sssubtlety.discontinuous_beacon_beams.DummyStainable;
import net.sssubtlety.discontinuous_beacon_beams.mixin_accessors.BeaconBlockEntityMixinAccessor;
import net.sssubtlety.discontinuous_beacon_beams.mixin_accessors.BeamSegmentMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.invoke.arg.Args;

import java.util.Arrays;
import java.util.List;

import static net.sssubtlety.discontinuous_beacon_beams.DiscontinuousBeaconBeamsConfig.*;

@Mixin(BeaconBlockEntity.class)
public abstract class BeaconBlockEntityMixin extends BlockEntity implements BeaconBlockEntityMixinAccessor {
    private static final DummyStainable DUMMY_STAINABLE = new DummyStainable();
    private static final float[] WHITE = DyeColor.WHITE.getColorComponents();

    @Shadow private final List<BeaconBlockEntity.BeamSegment> field_19178;

    private float[] colorOverride;
    private boolean hasColor;
    private boolean beamIsVisible;

    public BeaconBlockEntityMixin(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
        throw new IllegalStateException("BeaconBlockEntityMixin's dummy constructor called!");
    }

    @ModifyVariable(method = "tick", at = @At(value = "INVOKE_ASSIGN",
            target = "Lnet/minecraft/block/BlockState;getBlock()Lnet/minecraft/block/Block;"))
    private static Block blockAssessment(Block block, World world, BlockPos pos, BlockState state, BeaconBlockEntity beaconBlockEntity) {
        if (!enableBeaconBeams()) return block;
        BeaconBlockEntity.BeamSegment beamSegment = getTopSegment(beaconBlockEntity);
        if (beamSegment == null) { // inside beacon block, initialize
            ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).hasColor = false;
            ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).beamIsVisible = true;
            return block;
        }
        boolean currentlyInvisible = !((BeamSegmentMixinAccessor)beamSegment).isVisible();
        ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).beamIsVisible = !currentlyInvisible;
        if (currentlyInvisible) {
            if (hidingBlocksToggleBeams() && doesHideBeam(block)) {
                ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).beamIsVisible = true;
                if (rememberColorWhenHidingBlocksEnableBeams()) {
                    block = DUMMY_STAINABLE;
                    ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).colorOverride = beamSegment.getColor();
                    // hasColor goes unchanged
                } else {
                    block = DUMMY_STAINABLE;
                    ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).colorOverride = WHITE;
                    ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).hasColor = false;

                }
            } else if (stainableBlocksEnableBeams()) {
                if (block instanceof Stainable stainable) {
                    ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).colorOverride =
                            ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).hasColor &&
                            rememberColorWhenStainableBlocksEnableBeams() ?
                                    null : stainable.getColor().getColorComponents();
                    ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).beamIsVisible = true;
                    ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).hasColor = true;
                } else { // prevent new segment with non-stainable block
                    block = Blocks.GLASS;
                }
            }// else staying invisible
        } else {
            if (doesHideBeam(block)) {
                block = DUMMY_STAINABLE;
                ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).beamIsVisible = false;
                ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).colorOverride =
                        ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).hasColor ?
                                beamSegment.getColor() : null;
            } else if (block instanceof Stainable stainable) {
                ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).colorOverride =
                        ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).hasColor ?
                                null : stainable.getColor().getColorComponents();
                ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).hasColor = true;
            }
        }
        return block;
    }

    @ModifyArgs(method = "tick", at = @At(value = "INVOKE", target = "Ljava/util/List;add(Ljava/lang/Object;)Z"))
    private static void beamModifier(Args args, World world, BlockPos pos, BlockState state, BeaconBlockEntity beaconBlockEntity) {
        if (!enableBeaconBeams()) return;
        Object beamSegment = args.get(0);
        if (getTopSegment(beaconBlockEntity) == null) return;

        if (!((BeaconBlockEntityMixin) (Object) beaconBlockEntity).beamIsVisible)
            ((BeamSegmentMixinAccessor) beamSegment).setInvisible();

        if (((BeaconBlockEntityMixin) (Object) beaconBlockEntity).colorOverride != null)
            ((BeamSegmentMixinAccessor)beamSegment).setColor(((BeaconBlockEntityMixin) (Object) beaconBlockEntity).colorOverride);
    }


    @Redirect(method = "tick", at = @At(value = "INVOKE", target = "Ljava/util/Arrays;equals([F[F)Z"))
    private static boolean equalAndNotTogglingVisibility(float[] colorA, float[] colorB, World world, BlockPos pos, BlockState state, BeaconBlockEntity beaconBlockEntity) {
        if (!enableBeaconBeams()) return true;
        BeaconBlockEntity.BeamSegment beamSegment = getTopSegment(beaconBlockEntity);
        if (
                ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).beamIsVisible !=
                (beamSegment == null || ((BeamSegmentMixinAccessor) beamSegment).isVisible())
        ) // toggling visibility
            return false;

        return Arrays.equals(colorA, colorB);
    }

    private static BeaconBlockEntity.BeamSegment getTopSegment(BeaconBlockEntity beaconBlockEntity) {
        int size = ((BeaconBlockEntityMixin) (Object) beaconBlockEntity).field_19178.size();
        return size < 1 ? null : ((BeaconBlockEntityMixin)(Object)beaconBlockEntity).field_19178.get(size - 1);
    }

    @Override
    public boolean beamIsVisible() {
        return beamIsVisible;
    }
}
